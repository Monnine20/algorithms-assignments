package utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Stack;

/**
 * Class implementing a DFS search through a graph. It expects to be
 * provided with a "visitor" object which performs a suitable task
 * on each visited vertex.
 * @author Charilaos Skiadas
 *
 */
public class DFS {
	/**
	 * Carries out a simple DFS run and just prints results.
	 */
	private static class SimpleDFS {
		private SimpleGraph graph;         // The graph to be traversed
		private int order;                 // The graph order, stored separately
		public boolean[] visited;          // Keeps track of visited vertices
		public int[] parent;               // The "parent" of a vertex in the DFS tree 
		public List<Integer> pushOrder;  // The vertices in the order in which they are placed in the stack
		public List<Integer> popOrder;   // The vertices in the order in which they become dead-ends (get popped from the stack)
		private Stack<Integer> stack;     // Maintains the vertices in stack form 

		public SimpleDFS(SimpleGraph graph) {
			this.graph = graph;
			this.order = graph.order();
		}
		
		public void run() {
			// TODO : Initialize the visited array, create a parent array full of -1s, create empty pushOrder and popOrder lists, and create a new empty stack
			visited = new boolean[this.order];
			parent = new int[this.order];
			Arrays.fill(parent, -1);
			pushOrder = new ArrayList();
			popOrder = new ArrayList();
			stack = new Stack();
			
			// Now we start the run
			for (int startVertex = 0; startVertex < this.order; startVertex += 1) {
				if (!visited[startVertex]) {
					System.out.format("NEW START VERTEX: %d\n", startVertex);
					// We start a dfs from this root vertex
					this.dfs(startVertex);
				}
			}
      		// We arrived at the end of the array of vertices without new start point
			System.out.println("DONE!");
			this.printOrders();
		}
		public void dfs(int currentVertex) {
			System.out.format("Visiting vertex: %d\n", currentVertex);
			// TODO : We must visit the currentVertex. In order to do that:
			// 1. Insert the currentVertex in the stack
			stack.push(currentVertex);
			// 2. Add the currentVertex to the end of the pushOrder list
			pushOrder.add(pushOrder.size(), currentVertex);
			// 3. Mark it as visited
			visited[currentVertex] = true;
			
			// Now we must recurse over the neighboring vertices
			for (Integer nextVertex : this.graph.outgoingEdges(currentVertex)) {
				if (this.parent[currentVertex] != nextVertex) {
					// This is an actual new edge and not the one we just traversed.
					if (visited[nextVertex]) {
						// We encountered a back edge
						System.out.format("Back edge: %d - %d\n", nextVertex, currentVertex);
					} else {
						// We encountered a tree edge 
						System.out.format("Tree edge: %d - %d\n", currentVertex, nextVertex);
						// TODO : 
						// 1. adjust the parent index
						parent[nextVertex] = currentVertex;
						// 2. recursively call dfs on this vertex
						dfs(nextVertex);
					}
				}
			}
			// TODO: We are done with currentVertex. We must:
			// 1. Pop the currentVertex from the stack
			stack.pop();
			// 2. Add the currentVertex to the end of the popOrder list
			popOrder.add(currentVertex);
			
			
			System.out.format("Done visiting vertex (dead-end): %d\n", currentVertex);
		}

		/**
		 * Print the push and pop orders of the vertices
		 */
		private void printOrders() {
			System.out.print("PUSH ORDER: ");
			DFS.printList(Arrays.asList(pushOrder));
			System.out.println();
			System.out.print("POP ORDER: ");
			DFS.printList(Arrays.asList(popOrder));
			System.out.println();
		}
	}
		

	
	
	/**
	 * ComponentsDFS
	 * 
	 * Performs DFS search and collects the connected components of the graph
	 */
	private static class ComponentsDFS {
		private SimpleGraph graph;      // The graph to be traversed
		private int order;              // The graph order, stored separately
		public boolean[] visited;       // Keeps track of visited vertices
		public int[] parent;            // The "parent" of a vertex in the DFS tree 
		public List<Set<Integer>> components;    // The components to be returned 

		public ComponentsDFS(SimpleGraph graph) {
			this.graph = graph;
			this.order = graph.order();
		}
		
		public List<Set<Integer>> run() {
			// TODO : Initialize the visited array, create a parent array full of -1s, and initialize the components list as an empty list
			visited = new boolean[this.order];
			parent = new int[this.order];
			Arrays.fill(parent, -1);
			components = new ArrayList();
			// Now we start the run
			for (int startVertex = 0; startVertex < this.order; startVertex += 1) {
				if (!visited[startVertex]) {
					// TODO : A new start vertex means a new connected component. 
					// 1. Create a new empty Set<Integer> element
					Set<Integer> emptyset = new HashSet();
					// 2. Insert this new set element into the components list. 
					components.add(emptyset);
					// You don't need to insert the vertex in it, this will happen in the dfs method
					
					// We start a dfs from this root vertex
					this.dfs(startVertex);
				}
			}
      		// We arrived at the end of the array of vertices without new start point
			this.printComponents();
			
			return components;
		}

		/**
		 *  Carries out a dfs search starting from the given vertex
		 * @param currentVertex    the vertex to descend from.
		 */
		public void dfs(int currentVertex) {
			// TODO : We must visit the currentVertex. 
			// 1. Mark the vertex as visited
			visited[currentVertex] = true;
			// 2. Add the vertex to the newest component in the components list
			//components.add(components.size(), new HashSet(currentVertex));
			components.get(components.size()-1).add(currentVertex);
			
			// Now we must recurse over the neighboring vertices
			for (Integer nextVertex : this.graph.outgoingEdges(currentVertex)) {
				if (this.parent[currentVertex] != nextVertex) {
					// This is an actual new edge and not the one we just traversed.
					if ( ! visited[nextVertex]) {
						// We encountered a tree edge. This reaches a new vertex that is part of the same component. 
						// TODO : 
						// 1. adjust the parent index
						parent[nextVertex] = currentVertex;
						// 2. recursively call dfs on this vertex
						dfs(nextVertex);
					}
					// No else clause needed, as we do not need to do anything on back edges.
				}
			}
			// Nothing else to do
		}

		/**
		 * Print the various connected components
		 */
		private void printComponents() {
			System.out.format("Found %d components:\n", this.components.size());
			for (int i = 0; i < this.components.size(); i += 1) {
				Set<Integer> component = this.components.get(i);
				System.out.format("%d : ", i + 1);
				DFS.printList(component);
				System.out.println();
			}
		}
	}
	

	/**
	 * CycleDFS
	 * 
	 * Performs DFS search and to find a cycle.
	 */
	private static class CycleDFS {
		private SimpleGraph graph;      // The graph to be traversed
		private int order;              // The graph order, stored separately
		public boolean[] visited;       // Keeps track of visited vertices
		public int[] parent;            // The "parent" of a vertex in the DFS tree 
		public List<Integer> cycle;   // The final cycle, if there is one. Empty if there is no cycle
		
		public CycleDFS(SimpleGraph graph) {
			this.graph = graph;
			this.order = graph.order();
		}
		
		public List<Integer> run() {
			// TODO : Initialize the visited array, create an empty list for the cycle, and initialize the parent array with -1 values
			visited = new boolean[this.order];
			cycle = new ArrayList();
			parent = new int[this.order];
			Arrays.fill(parent, -1);
			// Now we start the run
			for (int startVertex = 0; startVertex < this.order; startVertex += 1) {
				if (!visited[startVertex]) {
					// Nothing special to do for new vertices
					// We start a dfs from this root vertex
					this.dfs(startVertex);
				}
			}
      		// We arrived at the end of the array of vertices without new start point
			this.printCycle();
			
			return cycle;
		}

		/**
		 *  Carries out a dfs search starting from the given vertex
		 * @param currentVertex    the vertex to descend from.
		 */
		public void dfs(int currentVertex) {
			// TODO : Must mark the vertex as visited
			visited[currentVertex] = true;
			// We must recurse over the neighboring vertices
			for (Integer nextVertex : this.graph.outgoingEdges(currentVertex)) {
				// TODO : If the cycle list is non-empty then simply return since we found a cycle 
				if (!cycle.isEmpty()) {
					return;
				}
				if (this.parent[currentVertex] != nextVertex) {
					// This is an actual new edge and not the one we just traversed.
					if ( ! visited[nextVertex]) {
						// We encountered a tree edge. 
						// TODO : This new vertex should have our currentVertex as a parent. 
						// 1. Set the parent array entry accordingly.
						parent[nextVertex] = currentVertex;
						// 2. Then recursively call dfs on this new vertex
						dfs(nextVertex);
					} else {
						// A back edge encountered. Means that nextVertex is an ancestor of currentVertex. 
						// We therefore have a cycle consisting of the ancestor path via the parent array, from currentVertex up to next Vertex
						// followed by this back edge we just discovered. We must record this cycle.
						// TODO : Start from currentVertex. Traverse the parent links and add the vertices into the cycle list as you go, until you arrive
						// at the nextVertex.
						for(int i = currentVertex; i < nextVertex; i++) {
							cycle.add(i);
						}
						// Once this cycle is recorded, there is no need to continue traversing the graph. We start returning.
						return;
					}
				}	
			}
			// Nothing else to do
		}

		/**
		 * Print the cycle if one was found, or a message otherwise
		 */
		private void printCycle() {
			if (this.cycle.isEmpty()) {
				System.out.println("The graph is acyclic");
			} else {
				System.out.print("Found cycle: ");
				DFS.printList(this.cycle);
				System.out.println();
			}
		}
	}
	


	/**
	 * BipartiteDFS
	 * 
	 * Performs DFS search to attempt to determine whether the graph is bipartite.
	 */
	private static class BipartiteDFS {
		private SimpleGraph graph;   // The graph to be traversed
		private int order;           // The graph order, stored separately
		public boolean[] visited;    // Keeps track of visited vertices
		public int[] parent;            // The "parent" of a vertex in the DFS tree 
		public int[] colors;         // One of two "colors" used for the two bipartite sets of vertices 
		public boolean failed;       // True if the attempt to use two colors failed
		
		public BipartiteDFS(SimpleGraph graph) {
			this.graph = graph;
			this.order = graph.order();
		}
		
		public void run() {
			// TODO : Initialize the visited and colors arrays, initialize the parent array with -1 values, and initialize the failed boolean to false
			visited = new boolean[this.order];
			colors = new int[this.order];
			Arrays.fill(colors, 0);
			parent = new int[this.order];
			Arrays.fill(parent, -1);
			failed = false;
			// Now we start the run
			for (int startVertex = 0; startVertex < this.order; startVertex += 1) {
				if (!visited[startVertex]) {
					// TODO : A brand new root vertex can have any color. Assign color 1 to it.
					colors[startVertex] = 1;

					// We start a dfs from this root vertex
					this.dfs(startVertex);
				}
			}
      		// We arrived at the end of the array of vertices without new start point
			this.printSets();
		}

		/**
		 * Carries out a dfs search starting from the given vertex
		 * @param currentVertex    the vertex to descend from.
		 */
		public void dfs(int currentVertex) {
			// TODO : Mark the vertex as visited
			visited[currentVertex] = true;
			// We must recurse over the neighboring vertices
			for (Integer nextVertex : this.graph.outgoingEdges(currentVertex)) {
				// TODO : If we have failed, then return right away. 
				if (failed == true) {
					return;
				}
				if (this.parent[currentVertex] != nextVertex) {
					// This is an actual new edge and not the one we just traversed.
					if ( ! visited[nextVertex]) {
						// We encountered a tree edge. 
						// TODO : This new vertex should be assigned a color opposite to the one of our current vertex. 
						// 1. adjust the parent index
						parent[nextVertex] += 1;
						// 2. If the currentVertex has color 1, set the color of nextVertex to 2.  
						if(colors[currentVertex] == 1) {
							colors[nextVertex] = 2;
						}
						// 3. If instead the currentVertex has color 2, set the color of nextVertex to 1.
						// 		You can achieve both 2 and 3 in one formula: If x is the one color, then 3-x is the other color.
						if(colors[currentVertex] == 2) {
							colors[nextVertex] = 1;
						}
						// 4. Recursively call dfs on nextVertex
						dfs(nextVertex);
						

					} else {
						// A back edge encountered. This may be a conflict if the colors are not compatible. 
						// TODO : If currentVertex and nextVertex have the same color, then our attempt has failed. Set the `failed` field accordingly.
						if(colors[currentVertex] == colors[nextVertex]) {
							failed = true;
						}

					}
				}
			}
			// Nothing else to do
		}

		/**
		 * Print the two sets that were found, or an error message
		 */
		private void printSets() {
			if (this.failed) {
				System.out.println("The graph is NOT bipartite");
			} else {
				System.out.println("The graph is bipartite! The parts are: ");
				List<Integer> color1 = new ArrayList<Integer>();
				List<Integer> color2 = new ArrayList<Integer>();
				for (int i = 0; i < this.colors.length; i += 1) {
					if (this.colors[i] == 1) {
						color1.add(i);
					} else {
						color2.add(i);
					}
				}
				System.out.print("Part 1: ");
				DFS.printList(color1);
				System.out.println();
				System.out.print("Part 2: ");
				DFS.printList(color2);
				System.out.println();
			}
		}
	}
	


	
	// Helper method for printing a list of items separated by commas
	// You do not need to change anything here
	private static <T> void printList(Iterable<T> list) {
		Iterator<T> iterator = list.iterator();
		if (! iterator.hasNext()) {
			return;
		}
		System.out.print(iterator.next());
		while (iterator.hasNext()) {
			System.out.print(", ");
			System.out.print(iterator.next());
		}
	}

	
	public static void main(String[] args) {
		// This is the graph from figure 3.10, with the numbers 0, ..., 9 corresponding to the letters a, ..., j
		SimpleGraph g1 = new SimpleGraph(10);
		g1.addBidirectionalEdge(0, 2);      // a-c
		g1.addBidirectionalEdge(0, 3);      // a-d
		g1.addBidirectionalEdge(0, 4);      // a-e
		g1.addBidirectionalEdge(2, 3);      // c-d
		g1.addBidirectionalEdge(1, 4);      // b-e   
		g1.addBidirectionalEdge(1, 5);      // b-f
		g1.addBidirectionalEdge(2, 5);      // c-f   
		g1.addBidirectionalEdge(4, 5);      // e-f
		g1.addBidirectionalEdge(6, 7);      // g-h
		g1.addBidirectionalEdge(7, 8);      // h-i   
		g1.addBidirectionalEdge(8, 9);      // i-j   
		g1.addBidirectionalEdge(6, 9);      // g-j   

		// Simple DFS
		SimpleDFS simpleDFS = new SimpleDFS(g1);
		simpleDFS.run();    
		// Push order should be: a, c, d, f, b, e, g, h, i, j
		// Pop order should be: d, e, b, f, c, a, j, i, h, g
		// Back edges: d-a, e-f, e-a, j-g
		
		// Components DFS
		ComponentsDFS componentsDFS = new ComponentsDFS(g1);
		componentsDFS.run();
		// Components should be a, b, c, d, e, f   and g, h, i, j
		
		CycleDFS cycleDFS = new CycleDFS(g1);
		cycleDFS.run();
		// A cycle based on one of the back edges should have been printed out
		
		BipartiteDFS bipartiteDFS = new BipartiteDFS(g1);
		bipartiteDFS.run();
		// This should have been a failed search.
		
		// A small square graph to show bipartite in action. Should find two sets, 0, 2 and 1, 3
		SimpleGraph g2 = new SimpleGraph(4);
		g2.addBidirectionalEdge(0, 1); 
		g2.addBidirectionalEdge(1, 2); 
		g2.addBidirectionalEdge(2, 3); 
		g2.addBidirectionalEdge(3, 0);
		BipartiteDFS bipartiteDFS2 = new BipartiteDFS(g2);
		bipartiteDFS2.run();
	}
}

	



