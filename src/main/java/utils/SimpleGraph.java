package utils;

import java.util.ArrayList;

/**
 * Class representing a simple implementation of
 * unweighted directed graphs.
 * Vertices are simply integers. We store the edges in
 * adjacency list format.
 * @author Charilaos Skiadas
 *
 */
public class SimpleGraph {
	public DLList<Integer>[] edges; 
	/**
	 * Create an empty simple graph with specified order
	 */
	public SimpleGraph(int order) {
		this.edges = (DLList<Integer>[]) new DLList[order];
		for (int i = 0; i < this.edges.length; i += 1) {
			this.edges[i] = new DLList<Integer>();
		}
	}

	/**
	 * Add an edge from one vertex to another, if one does not already exist.
	 * @param i     the source vertex
	 * @param j     the target vertex
	 */
	public void addEdge(int i, int j) {
		this.checkValidVertex(i);
		this.checkValidVertex(j);

		if (!this.edges[i].contains(j)) {
			this.edges[i].add(j);
		}
	}
	
	public void addBidirectionalEdge(int i, int j) {
		this.addEdge(i, j);
		this.addEdge(j, i);
	}
	
	public int order() {
		return this.edges.length;
	}
	
	public DLList<Integer>outgoingEdges(int i) {
		this.checkValidVertex(i);
		
		return this.edges[i]; // Note: for safety we should have cloned
	}
	/**
	 * Check if the vertex index is a valid index for the graph.
	 * @param i     the vertex index
	 * @throws IndexOutOfBoundsException
	 */
	private void checkValidVertex(int i) {
		if (i < 0 || i >= this.edges.length) {
			throw new IndexOutOfBoundsException(String.format("%d",i));
		}
	}
}
